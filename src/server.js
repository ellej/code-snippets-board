import dotenv from 'dotenv'
import express from 'express'

import log from './startup/logging.js'
import connectView from './startup/view.js'
import registerRoutes from './startup/routes.js'
import connectDB from './startup/db.js'
import prepareProd from './startup/prod.js'

dotenv.config()

const app = express()

log(app)
connectView(app)
registerRoutes(app)
connectDB()
prepareProd(app)

const port = process.env.PORT || 3000
app.listen(port, () => console.log(`Server running on port ${port}...`))
