import express from 'express'

import useInputValidator from '../middleware/useInputValidator.js'
import { validate } from '../models/user.js'
import { ROUTE_SIGNUP, ROUTE_LOGIN, ROUTE_LOGOUT } from '../config/routes.js'
import {
  getSignupForm,
  signUp,
  getLoginForm,
  logIn,
  logOut
} from '../controllers/auth.js'

const router = express.Router()

router.get(ROUTE_SIGNUP, getSignupForm)
router.post(ROUTE_SIGNUP, useInputValidator(validate, 'pages/signup'), signUp)

router.get(ROUTE_LOGIN, getLoginForm)
router.post(ROUTE_LOGIN, useInputValidator(validate, 'pages/login'), logIn)

router.post(ROUTE_LOGOUT, logOut)

export default router
