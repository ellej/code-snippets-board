import express from 'express'

import { index, home } from '../controllers/home.js'
import { ROUTE_HOME } from '../config/routes.js'

const router = express.Router()

router.get('/', index)
router.get(ROUTE_HOME, home)

export default router
