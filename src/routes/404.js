/**
 * Handle requests to non-existent routes.
 *
 * @param {object} req - Request object
 * @param {object} res - Response object
 * @param {Function} next - Middleware function to pass the request on
 */
const handle404NotFound = (req, res, next) => {
  const err = new Error(`Route could not be found: ${req.url}.`)
  err.status = 404
  err.pageToShow = 'errors/404'

  next(err)
}

export default handle404NotFound
