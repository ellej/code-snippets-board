import express from 'express'

import { checkLoggedIn, checkAuthorized } from '../middleware/auth.js'
import { validate } from '../models/snippet.js'
import useInputValidator from '../middleware/useInputValidator.js'
import checkUserExists from '../middleware/userExists.js'
import validateObjectId from '../middleware/validateObjectId.js'
import handleNotImplemented from '../controllers/notImplemented.js'
import { ROUTE_SNIPPET_CREATE, ROUTE_SNIPPET_UPDATE, ROUTE_SNIPPET_DELETE } from '../config/routes.js'
import {
  getAll,
  getCreateForm,
  create,
  getUpdateForm,
  update,
  remove
} from '../controllers/snippets.js'

const router = express.Router({ mergeParams: true })

router.all('*', checkLoggedIn)

router.get('/', [validateObjectId, checkUserExists, checkAuthorized], getAll)

router.route(ROUTE_SNIPPET_CREATE)
  .all(validateObjectId, checkUserExists, checkAuthorized)
  .get(getCreateForm)
  .post(useInputValidator(validate, 'pages/createSnippet'), create)

router.route(ROUTE_SNIPPET_UPDATE)
  .all(validateObjectId, checkUserExists, checkAuthorized)
  .get(getUpdateForm)
  .post(useInputValidator(validate, 'pages/updateSnippet'), update)

router.post(ROUTE_SNIPPET_DELETE, [validateObjectId, checkUserExists, checkAuthorized], remove)

// This application uses only GET and POST requests in order for it to
// work with JavaScript disabled on the client.
router.put('*', handleNotImplemented)
router.delete('*', handleNotImplemented)

export default router
