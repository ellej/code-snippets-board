/**
 * Handle requests for non-implemented verbs.
 *
 * @param {object} req - Request object
 * @param {object} res - Response object
 * @param {Function} next - Middleware function to pass the request on
 */
const handleNotImplemented = (req, res, next) => {
  // This application uses only GET and POST requests in order for it to
  // work with JavaScript disabled on the client.

  next(new Error('Not implemented.'))
}

export default handleNotImplemented
