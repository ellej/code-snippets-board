import User from '../models/user.js'
import { ROUTE_HOME } from '../config/routes.js'
import { FLASH_SUCCESS } from '../utils/flashTypes.js'

/**
 * Get the form for signing up a user.
 *
 * @param {object} req - Request object
 * @param {object} res - Response object
 * @param {Function} next - Middleware function to pass the request on
 * @returns {void} Nothing
 */
const getSignupForm = (req, res, next) => {
  if (_isLoggedIn(req)) {
    return res.redirect(ROUTE_HOME)
  }

  res.render('pages/signup')
}

/**
 * Sign up a user.
 *
 * @param {object} req - Request object
 * @param {object} res - Response object
 * @param {Function} next - Middleware function to pass the request on
 * @returns {void} Nothing
 */
const signUp = async (req, res, next) => {
  try {
    const usernameCaseInsensitive = new RegExp(`^${req.body.username}$`, 'i')
    let user = await User.findOne({ username: usernameCaseInsensitive })
    if (user) {
      return next({
        status: 400,
        message: 'Username is already taken.',
        pageToShow: 'pages/signup',
        flash: true
      })
    }

    // (input already validated in middleware)
    user = new User({
      username: req.body.username,
      password: req.body.password
    })
    await user.encryptPassword()
    await user.save()

    req.session.flash = { type: FLASH_SUCCESS, message: 'Success! Welcome aboard.' }
    req.session.user = { id: user._id, username: user.username }
    res.redirect(ROUTE_HOME)
  } catch (err) {
    next(err)
  }
}

/**
 * Get the form for logging in a user.
 *
 * @param {object} req - Request object
 * @param {object} res - Response object
 * @param {Function} next - Middleware function to pass the request on
 * @returns {void} Nothing
 */
const getLoginForm = (req, res, next) => {
  if (_isLoggedIn(req)) {
    return res.redirect(ROUTE_HOME)
  }

  res.render('pages/login')
}

/**
 * Log in a user.
 *
 * @param {object} req - Request object
 * @param {object} res - Response object
 * @param {Function} next - Middleware function to pass the request on
 * @returns {void} Nothing
 */
const logIn = async (req, res, next) => {
  try {
    const usernameCaseInsensitive = new RegExp(`^${req.body.username}$`, 'i')
    const user = await User.findOne({ username: usernameCaseInsensitive })
    if (!user) {
      return next({
        status: 400,
        message: 'Invalid username or password.',
        pageToShow: 'pages/login',
        flash: true
      })
    }

    const authenticated = await user.authenticate(req.body.password)
    if (!authenticated) {
      return next({
        status: 400,
        message: 'Invalid username or password.',
        pageToShow: 'pages/login',
        flash: true
      })
    }

    req.session.flash = { type: FLASH_SUCCESS, message: "Success! You're logged in." }
    req.session.user = { id: user._id, username: user.username }
    res.redirect(ROUTE_HOME)
  } catch (err) {
    next(err)
  }
}

/**
 * Log out a user.
 *
 * @param {object} req - Request object
 * @param {object} res - Response object
 * @param {Function} next - Middleware function to pass the request on
 */
const logOut = (req, res, next) => {
  delete req.session.user
  req.session.destroy(() => res.redirect(ROUTE_HOME))
}

/**
 * Check if the request is coming from a logged in user.
 *
 * @param {object} req - Request object
 * @returns {boolean} Whether or not the user is logged in
 */
const _isLoggedIn = (req) => {
  return !!(req.session && req.session.user)
}

export {
  getSignupForm,
  signUp,
  getLoginForm,
  logIn,
  logOut
}
