import User from '../models/user.js'
import Snippet from '../models/snippet.js'
import { ROUTE_HOME } from '../config/routes.js'
import { FLASH_SUCCESS } from '../utils/flashTypes.js'

/**
 * Get all the snippets.
 *
 * @param {object} req - Request object
 * @param {object} res - Response object
 * @param {Function} next - Middleware function to pass the request on
 */
const getAll = async (req, res, next) => {
  try {
    // (existence of user already checked in middleware)
    const user = await User.findById(req.params.id)

    const snippets = await Snippet
      .find({ _id: { $in: user.snippets } })
      .populate('user', { username: true })
      .sort({ createdAt: 'desc' })

    res.render('pages/index', {
      data: {
        user: req.session.user,
        snippets
      }
    })
  } catch (err) {
    next(err)
  }
}

/**
 * Get the form for creating a snippet.
 *
 * @param {object} req - Request object
 * @param {object} res - Response object
 * @param {Function} next - Middleware function to pass the request on
 */
const getCreateForm = (req, res, next) => {
  res.render('pages/createSnippet', {
    data: {
      user: req.session.user
    }
  })
}

/**
 * Create a snippet.
 *
 * @param {object} req - Request object
 * @param {object} res - Response object
 * @param {Function} next - Middleware function to pass the request on
 */
const create = async (req, res, next) => {
  try {
    // (existence of user already checked in middleware)
    const user = await User.findById(req.params.id)

    // (input already validated in middleware)
    const snippet = await Snippet.create({
      text: req.body.text,
      language: req.body.language,
      user: req.params.id
    })

    user.snippets.push(snippet._id)
    await user.save()

    req.session.flash = { type: FLASH_SUCCESS, message: 'Success! Snippet created.' }
    res.redirect(ROUTE_HOME)
  } catch (err) {
    next(err)
  }
}

/**
 * Get the form for updating a snippet.
 *
 * @param {object} req - Request object
 * @param {object} res - Response object
 * @param {Function} next - Middleware function to pass the request on
 * @returns {void} Nothing
 */
const getUpdateForm = async (req, res, next) => {
  try {
    const snippet = await Snippet.findById(req.params.snippetId)
    if (!snippet) {
      return next({
        status: 404,
        message: 'There is no snippet with the given ID.'
      })
    }

    res.render('pages/updateSnippet', {
      data: {
        user: req.session.user,
        snippet: {
          id: snippet._id,
          text: snippet.text,
          language: snippet.language
        }
      }
    })
  } catch (err) {
    next(err)
  }
}

/**
 * Update a snippet.
 *
 * @param {object} req - Request object
 * @param {object} res - Response object
 * @param {Function} next - Middleware function to pass the request on
 */
const update = async (req, res, next) => {
  try {
    // (input already validated in middleware)
    await Snippet.updateOne(
      { _id: req.params.snippetId },
      {
        $set: {
          text: req.body.text,
          language: req.body.language,
          updatedAt: new Date()
        }
      }
    )

    req.session.flash = { type: FLASH_SUCCESS, message: 'Success! Snippet updated.' }
    res.redirect(ROUTE_HOME)
  } catch (err) {
    next(err)
  }
}

/**
 * Remove a snippet.
 *
 * @param {object} req - Request object
 * @param {object} res - Response object
 * @param {Function} next - Middleware function to pass the request on
 */
const remove = async (req, res, next) => {
  try {
    await Snippet.deleteOne({ _id: req.params.snippetId })

    req.session.flash = { type: FLASH_SUCCESS, message: 'Success! Snippet deleted.' }
    res.redirect(ROUTE_HOME)
  } catch (err) {
    next(err)
  }
}

export {
  getAll,
  getCreateForm,
  create,
  getUpdateForm,
  update,
  remove
}
