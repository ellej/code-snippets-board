import Snippet from '../models/snippet.js'
import { ROUTE_HOME } from '../config/routes.js'

/**
 * Get the root page.
 *
 * @param {object} req - Request object
 * @param {object} res - Response object
 * @param {Function} next - Middleware function to pass the request on
 */
const index = (req, res, next) => {
  res.redirect(ROUTE_HOME)
}

/**
 * Get all the snippets for the home page.
 *
 * @param {object} req - Request object
 * @param {object} res - Response object
 * @param {Function} next - Middleware function to pass the request on
 */
const home = async (req, res, next) => {
  try {
    const isLoggedIn = req.session && req.session.user

    const snippets = await Snippet
      .find({})
      .populate('user', { username: true })
      .sort({ createdAt: 'desc' })

    res.render('pages/index', {
      data: {
        user: isLoggedIn ? req.session.user : null,
        snippets
      },
      flash: res.locals.flash
    })
  } catch (err) {
    next(err)
  }
}

export { index, home }
