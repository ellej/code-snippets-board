export const ROUTE_HOME = '/home'

export const ROUTE_AUTH_BASE = '/auth'
export const ROUTE_SIGNUP = '/signup'
export const ROUTE_LOGIN = '/login'
export const ROUTE_LOGOUT = '/logout'

export const ROUTE_USER_SNIPPETS_BASE = '/users/:id/snippets'
export const ROUTE_SNIPPET_CREATE = '/create'
export const ROUTE_SNIPPET_UPDATE = '/:snippetId/update'
export const ROUTE_SNIPPET_DELETE = '/:snippetId/delete'
