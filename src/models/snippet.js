import mongoose from 'mongoose'
import Joi from 'joi'
import joiObjectId from 'joi-objectid'

import User from './user.js'
import { SNIPPET_COLLECTION, USER_COLLECTION } from '../utils/collections.js'

const Schema = mongoose.Schema

const snippetSchema = new Schema({
  text: {
    type: String,
    minlength: 1,
    maxlength: 1024,
    required: true
  },
  language: {
    type: String,
    minlength: 1,
    maxlength: 50,
    lowercase: true,
    trim: true,
    required: true
  },
  user: {
    type: Schema.Types.ObjectId,
    ref: USER_COLLECTION
  },
  createdAt: {
    type: Date,
    default: Date.now
  },
  updatedAt: {
    type: Date
  }
})

snippetSchema.pre('deleteOne', async function () {
  // pre('deleteOne') hooks into the Query so 'this' refers to
  // the query and not the document. Thus, we first need to
  // query the actual document being deleted
  const snippet = await this.model.findOne(this.getQuery())

  // Delete the snippet reference from the user
  await User.updateOne(
    { _id: snippet.user },
    { $pull: { snippets: snippet._id } }
  )
})

const Snippet = mongoose.model(SNIPPET_COLLECTION, snippetSchema)

Joi.objectId = joiObjectId(Joi)

/**
 * Validate user inputs for the snippet.
 *
 * @param {object} snippet - The snippet to validate
 * @returns {object} Object containing a value field and an error field if any
 */
const validate = (snippet) => {
  const schema = Joi.object({
    text: Joi.string()
      .min(1).message('Code must be at least 1 character.')
      .max(1024).message('Code must be at most 1024 characters.')
      .required(),

    language: Joi.string()
      .trim()
      .lowercase()
      .min(1).message('Language must be at least 1 character.')
      .max(50).message('Language must be at most 50 characters.')
      .required(),

    user: Joi.objectId(),
    createdAt: Joi.date().default(Date.now),
    updatedAt: Joi.date()
  })

  return schema.validate(snippet)
}

export { validate }
export default Snippet
