import bcrypt from 'bcrypt'
import mongoose from 'mongoose'
import Joi from 'joi'
import joiObjectId from 'joi-objectid'

import { USER_COLLECTION, SNIPPET_COLLECTION } from '../utils/collections.js'

const Schema = mongoose.Schema

const userSchema = new Schema({
  username: {
    type: String,
    minlength: 4,
    maxlength: 50,
    trim: true,
    unique: true,
    required: true
  },
  password: {
    type: String,
    minlength: 7,
    maxlength: 1024,
    required: true
  },
  snippets: {
    type: [Schema.Types.ObjectId],
    ref: SNIPPET_COLLECTION
  }
})

userSchema.methods = {
  /**
   * Encrypt a password.
   */
  encryptPassword: async function () {
    const SALT_ROUNDS = 10
    const salt = await bcrypt.genSalt(SALT_ROUNDS)
    const encrypted = await bcrypt.hash(this.password, salt)
    this.password = encrypted
  },

  /**
   * Compare a plaintext password with the already encrypted one.
   *
   * @param {string} plaintextPassword - The password in plaintext
   * @returns {Promise<boolean>} Promise that resolves to whether or not they match
   */
  authenticate: function (plaintextPassword) {
    return bcrypt.compare(plaintextPassword, this.password)
  }
}

const User = mongoose.model(USER_COLLECTION, userSchema)

Joi.objectId = joiObjectId(Joi)

/**
 * Validate user inputs for the user.
 *
 * @param {object} user - The user to validate
 * @returns {object} Object containing a value field and an error field if any
 */
const validate = (user) => {
  const schema = Joi.object({
    username: Joi.string()
      .trim()
      .min(4).message('Username must be at least 4 characters.')
      .max(50).message('Username must be at most 50 characters.')
      .required(),

    password: Joi.string()
      .min(7).message('Password must be at least 7 characters.')
      .max(255).message('Password is too long.')
      .required(),

    snippets: Joi.array()
      .items(Joi.objectId())
      .unique()
  })

  return schema.validate(user)
}

export { validate }
export default User
