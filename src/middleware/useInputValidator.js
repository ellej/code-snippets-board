/**
 * A validator wrapper so that custom input validators
 * can be used as middleware.
 *
 * @param {Function} validator - The validator
 * @param {string} currentPage - The page using the input form
 * @returns {Function} Function wrapped around the validator
 */
const useInputValidator = (validator, currentPage) => {
  return (req, res, next) => {
    const { error } = validator(req.body)
    if (error) {
      return next({
        status: 400,
        message: error.details[0].message, // specific to the Joi library
        pageToShow: currentPage,
        flash: true
      })
    }

    next()
  }
}

export default useInputValidator
