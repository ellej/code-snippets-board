import Snippet from '../models/snippet.js'

/**
 * Check if the user is authenticated (is logged in).
 *
 * @param {object} req - Request object
 * @param {object} res - Response object
 * @param {Function} next - Middleware function to pass the request on
 * @returns {void} Nothing
 */
const checkLoggedIn = (req, res, next) => {
  if (!req.session || !req.session.user) {
    return next({
      status: 401,
      message: 'Access denied. No session.'
    })
  }

  next()
}

/**
 * Check if the request is authorized (if user is allowed access).
 *
 * @param {object} req - Request object
 * @param {object} res - Response object
 * @param {Function} next - Middleware function to pass the request on
 * @returns {void} Nothing
 */
const checkAuthorized = async (req, res, next) => {
  if (req.session.user.id !== req.params.id) {
    return next({
      status: 403,
      message: 'Access denied. Invalid session.'
    })
  }

  if (!req.params.snippetId) {
    return next()
  }

  const snippet = await Snippet.findById(req.params.snippetId)
  if (!snippet) {
    return next({
      status: 404,
      message: 'There is no snippet with the given ID.'
    })
  }

  if (req.params.id !== snippet.user.toString()) {
    return next({
      status: 403,
      message: 'Access denied. Invalid session.'
    })
  }

  next()
}

export { checkLoggedIn, checkAuthorized }
