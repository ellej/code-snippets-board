import mongoose from 'mongoose'

const isValid = mongoose.Types.ObjectId.isValid

/**
 * Check if an ID is a valid ObjectId.
 *
 * @param {object} req - Request object
 * @param {object} res - Response object
 * @param {Function} next - Middleware function to pass the request on
 * @returns {void} Nothing
 */
const checkValidObjectId = (req, res, next) => {
  const { id, snippetId } = req.params

  if ((id && !isValid(id)) || (snippetId && !isValid(snippetId))) {
    return next({
      status: 400,
      message: 'Invalid ID.'
    })
  }

  next()
}

export default checkValidObjectId
