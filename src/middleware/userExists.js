import User from '../models/user.js'

/**
 * Check if a user exists.
 *
 * @param {object} req - Request object
 * @param {object} res - Response object
 * @param {Function} next - Middleware function to pass the request on
 * @returns {void} Nothing
 */
const checkUserExists = async (req, res, next) => {
  const user = await User.findById(req.params.id)
  if (!user) {
    return next({
      status: 404,
      message: 'There is no user with the given ID.'
    })
  }

  next()
}

export default checkUserExists
