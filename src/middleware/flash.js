/**
 * Handle flash messages.
 *
 * @param {object} req - Request object
 * @param {object} res - Response object
 * @param {Function} next - Middleware function to pass on the request
 * @returns {void} Nothing
 */
const handleFlash = (req, res, next) => {
  // make flash messages available in the response before deleting it
  res.locals.flash = req.session.flash
  delete req.session.flash

  next()
}

export default handleFlash
