import { FLASH_DANGER } from '../utils/flashTypes.js'

/**
 * Handle errors in the request processing pipeline.
 *
 * @param {object} err - Response error object
 * @param {object} req - Request object
 * @param {object} res - Response object
 * @param {Function} next - Middleware function to pass on the request
 * @returns {void} Nothing
 */
const handleError = (err, req, res, next) => {
  err.status = err.status || 500
  err.message = err.message || 'Something went wrong...'

  const flash = err.flash && { type: FLASH_DANGER, message: err.message }
  const data = req.session && { user: req.session.user }
  const secondaryPage = process.env.NODE_ENV === 'production'
    ? 'errors/500'
    : 'errors/custom'

  res
    .status(err.status)
    .render(err.pageToShow || secondaryPage, { error: err, data, flash })
}

export default handleError
