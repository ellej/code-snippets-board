import mongoose from 'mongoose'

/**
 * Connect to the database.
 */
const connect = () => {
  if (process.env.NODE_ENV !== 'production') {
    // Enable logging collection methods + arguments to the console
    mongoose.set('debug', true)
  }

  const DB_URI = process.env.DB_URI
  mongoose.connect(DB_URI, {
    useNewUrlParser: true,
    useFindAndModify: false,
    useCreateIndex: true,
    useUnifiedTopology: true
  })

  const db = mongoose.connection
  db.on('connected', () => console.log('Connected to the database...'))
  db.on('disconnected', () => { throw new Error('Disconnected from the database...') })
  db.on('error', (err) => { throw err })
}

export default connect
