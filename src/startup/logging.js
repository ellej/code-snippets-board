import morgan from 'morgan'

/**
 * Log incoming requests.
 *
 * @param {object} app - The server application
 */
const log = (app) => {
  app.use(morgan('dev'))

  process
    .on('uncaughtException', (err) => logAndExit(err, 0))
    .on('unhandledRejection', (err) => logAndExit(err, 0))
}

/**
 * Log an error and exit the Node process.
 *
 * @param {Error} err - The error to log
 * @param {number} exitCode - The exit code to use
 */
const logAndExit = (err, exitCode) => {
  console.error(err)
  process.exit(exitCode)
}

export default log
