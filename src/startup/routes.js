import express from 'express'
import session from 'express-session'

import authRouter from '../routes/auth.js'
import homeRouter from '../routes/home.js'
import snippetsRouter from '../routes/snippets.js'
import notFound404 from '../routes/404.js'
import errorHandler from '../middleware/error.js'
import flash from '../middleware/flash.js'
import { ROUTE_AUTH_BASE, ROUTE_USER_SNIPPETS_BASE } from '../config/routes.js'

/**
 * Register the routes for the application.
 *
 * @param {object} app - The server application
 */
const registerRoutes = (app) => {
  app.use(express.urlencoded({ extended: false }))
  app.use(session(getSessionOptions()))
  app.use(flash)

  app.use(ROUTE_AUTH_BASE, authRouter)
  app.use(ROUTE_USER_SNIPPETS_BASE, snippetsRouter)
  app.use('/', homeRouter)
  app.use(notFound404)
  app.use(errorHandler)
}

/**
 * Get the session options.
 *
 * @returns {object} - The options
 */
const getSessionOptions = () => {
  const ONE_HOUR = 1000 * 60 * 60

  return {
    name: process.env.SESSION_NAME,
    secret: process.env.SESSION_SECRET,
    resave: false,
    saveUninitialized: false,
    cookie: {
      httpOnly: true,
      secure: !!(process.env.NODE_ENV === 'production'),
      sameSite: 'lax',
      maxAge: ONE_HOUR
    }
  }
}

export default registerRoutes
