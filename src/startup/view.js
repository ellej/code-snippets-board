import express from 'express'
import { dirname, join } from 'path'
import { fileURLToPath } from 'url'

const __filename = fileURLToPath(import.meta.url)
const __dirname = dirname(__filename)
const VIEW_ENGINE = 'pug'

/**
 * Connect the view engine and templates.
 *
 * @param {object} app - The server application
 */
const connectView = (app) => {
  app.set('views', join(__dirname, '..', 'views'))
  app.set('view engine', VIEW_ENGINE)
  app.use(express.static(join(__dirname, '..', '..', 'public')))
}

export default connectView
