# Code Snippets Board

A Node.js and Pug server-side rendered web application for users to sign up and post code snippets to a shared board. Authenticated users can perform all CRUD functionalities on their snippets. The application works even if the client has disabled JavaScript.

## Table of Contents

- [Tech Stack](#tech-stack)
- [Screenshot](#screenshot)
- [Getting Started](#getting-started)
  - [Prerequisites](#prerequisites)
  - [Clone the repo](#clone-the-repo)
  - [Install dependencies](#install-dependencies)
  - [Configure environment variables](#configure-environment-variables)
  - [Run the application](#run-the-application)

## Tech Stack

* Node.js
* Express
* Pug
* MongoDB

## Screenshot

![Code snippets app screenshot](https://res.cloudinary.com/ellej/image/upload/c_scale,f_auto,q_auto,w_1920/v1632078815/screenshots/app-code-snippets_s8c6mr.png)

## Getting Started

The following instructions explain how to run the application in **dev mode** locally.

### Prerequisites

This application requires that you have:

* [Node.js](https://nodejs.org/) installed
* [MongoDB](https://www.mongodb.com/try) installed or in the cloud (see below)

If you already have a [MongoDB Atlas](https://www.mongodb.com/cloud/atlas) (cloud) account or want your database to be hosted on their cloud **for free**, see **Option 1** below. If you would rather host your own MongoDB database locally on your machine, see **Option 2** below. 

##### MongoDB Option 1. Set up Atlas (cloud)

1. Go to the [register page](https://www.mongodb.com/cloud/atlas/register).
2. Create or login to your Atlas account.
3. Go to the [getting started guide](https://docs.atlas.mongodb.com/getting-started/).
4. Follow the guide in order to deploy a free cluster, create a database user, and get the connection string.

##### MongoDB Option 2. Set up Compass (local database with GUI)

A. Download MongoDB Compass
1. Go to the [download page](https://www.mongodb.com/try/download/compass).
2. Select the latest version of *Stable*.
3. Select the platform you are using.
4. Click *Download*.

B. Install MongoDB Compass and get it started
1. Go to the [installation guide](https://docs.mongodb.com/manual/administration/install-community/).
2. Select the guide appropriate for your operating system and follow the steps.

### Clone the repo

Open a terminal window and change the directory to where you want to place the cloned repo directory, then run:

```bash
# using ssh
git clone git@gitlab.com:ellej/code-snippets-board.git

# using https
git clone https://gitlab.com/ellej/code-snippets-board.git
```

### Install dependencies

```bash
cd code-snippets-board
npm install
```

### Configure environment variables

In the root directory of the project, create a file called `.env`.

This file will contain key-value pairs in the form of `<KEY>=<VALUE>` for each environment variable. You can view a sample of this in the file `sample.env`.

The following steps explain how to set the environment variables `PORT`, `DB_URI`, `SESSION_NAME`, and `SESSION_SECRET`.

##### Add the port to listen to

Decide which port (e.g. `3000`) you want the server to listen for incoming requests from.

In `.env`, set the port number as the value to the `PORT` key.

```shell
PORT=<YOUR PORT NUMBER>
```

##### Add the database connection string

If you have set up MongoDB as described in the [Prerequisites](#prerequisites) section, go ahead and add the URI (connection string) as the value to the `DB_URI` key in `.env`.

> Make sure to add a name for your database in the URI. For instance, if you are running MongoDB locally and want the database to be called `code_snippets_app`, the URI may be `mongodb://localhost/code_snippets_app`. (MongoDB will automatically create that database if it does not exist.)

```shell
DB_URI=<YOUR MONGODB DATABASE URI>
```

##### Add a name and secret for the session

This application uses cookies and sessions for implementing authentication and authorization.

In `.env`, add a name for the session ID as the value to the `SESSION_NAME` key. Also add a **randomly generated** string as the value to the `SESSION_SECRET` key that will be used for signing the session ID.

```shell
SESSION_NAME=<NAME OF SESSION ID>
SESSION_SECRET=<RANDOM STRING TO SIGN SESSION ID>
```

> **No** single or double quotes are needed for the environment variable values.

### Run the application

In a terminal window, navigate to the project's root directory, and run:

```bash
npm start
```

Open a web browser and navigate to `localhost` using the port number you configured in the [previous section](#add-the-port-to-listen-to). E.g. `http://localhost:3000`.

🥳 You can now sign up and start posting beautiful code snippets!
